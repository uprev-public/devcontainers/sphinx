FROM ubuntu:22.04 as dev_stage


ENV DEBIAN_FRONTEND noninteractive


RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \ 
    wget \ 
    cmake \
    curl \
    build-essential \
    openssl \
    make \ 
    git \
    python3 \
    python3-pip \
    python3-setuptools \
    plantuml \
    graphviz \
    texlive \
    latexmk \
    texlive-science \
    texlive-formats-extra \ 
    tex-gyre\
    Node.js \
    npm 

#Copy the latest release of plantuml. Ubuntu 18.04 package manager has an older one without timing diagram support
RUN wget https://github.com/plantuml/plantuml/releases/download/v1.2022.1/plantuml-1.2022.1.jar -O /usr/share/plantuml/plantuml.jar

#Set up node 16 
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash 
RUN . /root/.bashrc \
    && nvm install 16 \
    && nvm use 16 \
    && nvm alias default 16 \
    && npm install -g npm@latest


#sphinx dependencies 
RUN pip3 install sphinx sphinxcontrib-plantuml sphinxcontrib-youtube sphinx-rtd-theme recommonmark restview docxbuilder docxbuilder[math] sphinxcontrib-mermaid

RUN npm install -g @mermaid-js/mermaid-cli

#mrtutils 
RUN pip3 install -U mrtutils

#fixes error: ImportError: cannot import name 'soft_unicode' from 'markupsafe'
RUN pip3 install markupsafe==2.0.1
